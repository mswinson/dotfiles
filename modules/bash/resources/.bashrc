set -o vi
set -o noclobber
set +f

export TERM=xterm-color

for f in ~/.bash.d/*
do
  echo "loading bash module $f"
  source $f
done
