set nocompatible
filetype off

set rtp+=~/
set rtp+=~/.vim
set rtp+=~/.vim/bundle/Vundle.vim
set rtp+=.


source ~/.vim/plugins.vim
source ~/.vim/filetypes.vim
source ~/.vim/references.vim
source ~/.vim/layout.vim
source ~/.vim/highlighting.vim
source ~/.vim/tabbing.vim
source ~/.vim/keybindings.vim
source ~/.vim/errorformats.vim
source ~/.vim/modules


set history=1000
set autoread

set smartcase
set showmatch
set noerrorbells
set novisualbell

set encoding=utf8
set ffs=unix,mac,dos
set nobackup
set nowb
set noswapfile

set background=dark
let g:solarized_termcolors=256
let g:solarized_termtrans=1
colorscheme solarized

