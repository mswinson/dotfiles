" References
function NewReference()
  let filename="~/reference/".&ft
  execute 'new' filename
endfunction

command Reference :call NewReference()
