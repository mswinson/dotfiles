filetype indent on

set filetype=unknown
au BufNewFile,BufRead *.sh set filetype=sh
au BufNewFile,BufRead install set filetype=sh
au BufNewFile,BufRead COMMIT_EDITMSG set filetype=commitmessage spell textwidth=72
au BufNewFile,BufRead *.md set filetype=markdown
au BufNewFile,BufRead Makefile set filetype=make
au BufNewFile,BufRead *.hpp set filetype=cpp
au BufNewFile,BufRead *.cpp set filetype=cpp
au BufNewFile,BufRead *.json set filetype=javascript
au BufNewFile,BufRead *.html set filetype=html
au BufNewFile,BufRead Rakefile set filetype=ruby
au BufNewFile,BufRead *.thor set filetype=ruby
au BufNewFile,BufRead Gemfile set filetype=ruby
au BufNewFile,BufRead *.erb set filetype=ruby
au BufNewFile,BufRead .gitignore set filetype=gitignore
au BufNewFile,BufRead .feature set filetype=cucumber
au BufNewFile,BufRead Vagrantfile set filetype=vagrant
au BufNewFile,BufRead *.go set filetype=golang
